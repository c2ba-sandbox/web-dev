#!/bin/bash

CURRENT_DIR=`dirname "$0"`

for filename in $CURRENT_DIR/*.js; do
  MINIFIED=`npx minify $filename`
  echo "javascript:(function () { $MINIFIED })()" > "$filename.min"
done
