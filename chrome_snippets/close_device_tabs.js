for (const elmt of document.querySelectorAll("div.list.pages")) {
    if (elmt.children.length > 0) {
        pages = elmt
        break
    }
}
page_names = Array.from(pages.children).map(page => page.querySelector("div.name").innerHTML)
if (confirm(`Close these tabs ? ${page_names}`)) {
    for (const page of pages.children) {
        actions = Array.from(page.querySelectorAll("span.action")).filter(elmt => elmt.innerHTML == "close")
        actions[0].click()
    }
}