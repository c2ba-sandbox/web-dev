for (const elmt of document.querySelectorAll("div.list.pages")) {
    if (elmt.children.length > 0) {
        pages = elmt
        break
    }
}
console.log(pages)
tabs = Array.from(pages.children).map(page => ({
    "name": page.querySelector("div.name").innerHTML,
    "url": page.querySelector("div.url").innerHTML
}))
copy({
    "date": new Date().toISOString(),
    "tabs": tabs
})